import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container, Button } from 'reactstrap';
import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
} from '@coreui/react';
import DefaultHeader from './DefaultHeader';
import DefaultFooter from './DefaultFooter';
import navigation from '../_nav';
import routes from '../routes';

class DefaultLayout extends Component {

    _aliasesDisplayPicture = (firstname, lastname) => {
        // return `${firstname.substring(0, 1).toUpperCase()}${lastname.substring(0, 1).toUpperCase()}`
        return 'ADMIN'
    }

    render () {
        const _user = JSON.parse(localStorage.getItem('user'));
        return (
            <div className="app">
                <div>
                    <AppHeader fixed style={{ backgroundColor: 'yellow' }}>
                        <DefaultHeader {...this.props} />
                    </AppHeader>
                </div>
                <div className="app-body">
                    <AppSidebar fixed display="lg">
                        <AppSidebarHeader />
                        <AppSidebarForm />
                        <Suspense>
                            <AppSidebarNav navConfig={navigation} {...this.props} />
                        </Suspense>
                        <AppSidebarFooter />
                        <AppSidebarMinimizer />
                    </AppSidebar>
                    <main className="main">
                        <AppBreadcrumb appRoutes={routes}/>
                        <Container fluid>
                            <Switch>
                                {routes.map((route, idx) => {
                                    return route.component ? (
                                    <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <route.component {...props} />
                                        )} />
                                    ) : (null);
                                })}
                            </Switch>
                        </Container>
                    </main>
                </div>
                <AppFooter>
                    <DefaultFooter />
                </AppFooter>
            </div>
        );
    }
}

export default DefaultLayout;