import React from 'react';
import { Nav, Button, NavItem, NavLink, DropdownToggle, DropdownMenu, DropdownItem, Badge } from 'reactstrap';
import navigation from '../_nav';
import {
  AppNavbarBrand,
  AppHeaderDropdown
} from '@coreui/react'
require('dotenv').config();

class DefaultHeader extends React.Component {
    _aliasesDisplayPicture = (firstname, lastname) => {
        // return `${firstname.substring(0, 1).toUpperCase()}${lastname.substring(0, 1).toUpperCase()}`
        return 'ADMIN'
    }

    render () {
        const _user = JSON.parse(localStorage.getItem('user'));
        return (
            <React.Fragment style={{ backgroundColor: 'yellow' }}>
                <AppNavbarBrand> ACS PLB SISTEM </AppNavbarBrand>
                <Nav className="d-md-down-none" navbar>
                </Nav>
                <Nav className="ml-auto" navbar>
                    <Nav style={{ marginRight: 12 }}>
                        <span className="cui-user icons" style={{ marginRight: 5, marginTop: 4 }}/>{this._aliasesDisplayPicture()}</Nav>
                    <Nav style={{ marginRight: 12 }}>
                        <span className="cui-settings icons" style={{ marginRight: 5, marginTop: 4 }}/>Setting</Nav>
                    <Nav style={{ marginRight: 12 }}>
                        <span className="cui-lock-locked icons" style={{ marginRight: 5, marginTop: 4 }}/>Logout</Nav>
                </Nav>
            </React.Fragment>
        );
    }
}

export default DefaultHeader;