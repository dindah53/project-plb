import {  
    GET_DEPO,
    FAILED_GET_DEPO
    } from '../constants/Constants';
import { RequestHandler, RequestHandlerDelete } from '../../src/helpers/RequestHandler';
import ENVIRONMENT from '../../src/environments/environment';

export const failedGetDepo = (data) => {
    return {
        type: FAILED_GET_DEPO,
        data: data
    }
}

export const successGetDepo = (data) => {
    return {
        type: GET_DEPO,
        payload: data
    }
}

export const getListDepo = async (page, perPage) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/GetDaftarDepo?page=${page}&perPage=${perPage}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};

export const saveDepo = async (daftarDepo) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/PostDaftarDepo`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`}, daftarDepo);
};

export const deleteDepo = async (id) => {
    return await RequestHandlerDelete(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/DeleteDaftarDepo?Id=${id}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`});
};