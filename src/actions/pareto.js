import {  
    FAILED_USERINFO,
    REQUEST_SUMMARY_PARETO,
    SUCCESS_SUMMARY_PARETO,
    FAILED_SUMMARY_PARETO,
    REQUEST_PARETO,
    SUCCESS_PARETO,
    FAILED_PARETO,
    PARETO_DETAIL
    } from '../constants/Constants';
import { RequestHandler } from '../../src/helpers/RequestHandler';
import ENVIRONMENT from '../../src/environments/environment';

export const failedUserinfo = (data) => {
    return {
        type: FAILED_USERINFO,
        data: data
    }
}

export const paramParetoDetail = (data) => {
    return {
        type: PARETO_DETAIL,
        payload: data
    }
}

export const requestSummaryPareto = () => {
    return {
        type: REQUEST_SUMMARY_PARETO
    }
}

export const successSummaryPareto = (data) => {
    return {
        type: SUCCESS_SUMMARY_PARETO,
        data: data
    }
}

export const failedSummaryPareto = (data) => {
    return {
        type: FAILED_SUMMARY_PARETO,
        data: data
    }
}
export const requestPareto = () => {
    return {
        type: REQUEST_PARETO
    }
}

export const successPareto = (data) => {
    return {
        type: SUCCESS_PARETO,
        data: data
    }
}

export const failedPareto = (data) => {
    return {
        type: FAILED_PARETO,
        data: data
    }
}

export const getParetoSummary = async (date, plant, short) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL_OT_TDL}api/v1/summarypareto?Date=${date}&Plant=${plant}&Sortby=${short}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};

export const getPareto = async (remarks, date, plant, area) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL_OT_TDL}api/v1/pareto?Remarks=${remarks}&Date=${date}&Plant=${plant}&Area=${area}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
}
