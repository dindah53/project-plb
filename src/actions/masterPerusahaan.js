import {  
    GET_LIST_PERUSAHAAN
    } from '../constants/Constants';
import { RequestHandler, RequestHandlerDelete } from '../../src/helpers/RequestHandler';
import ENVIRONMENT from '../../src/environments/environment';

export const successGetPerusahaan = (data) => {
    return {
        type: GET_LIST_PERUSAHAAN,
        payload: data
    }
}

export const getListPerusahaan = async (page, perpage) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/GetDaftarPerusahaan?page=${page}&perPage=${perpage}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};

export const savePerushaan = async (valuePerusahaan) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/PostDaftarPerusahaan`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`}, valuePerusahaan);
};

export const deletePerusahaan = async (id) => {
    return await RequestHandlerDelete(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/DeleteDaftarPerusahaan?Id=${id}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`});
};