import {  
    GET_SHIPPINGLINE
    } from '../constants/Constants';
import { RequestHandler, RequestHandlerDelete } from '../../src/helpers/RequestHandler';
import ENVIRONMENT from '../../src/environments/environment';

export const successGetShippingline = (data) => {
    return {
        type: GET_SHIPPINGLINE,
        payload: data
    }
}

export const getListShippingline = async (page, perpage) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/GetDaftarShippingLine?page=${page}&perPage=${perpage}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};

export const saveShippingline = async (valueShip) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/PostDaftarShippingLine`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`}, valueShip);
};

export const deleteShippingline = async (id) => {
    return await RequestHandlerDelete(`${ENVIRONMENT.API_URL}/api/v1/daftardepo/DeleteDaftarShippingLine?Id=${id}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`});
};