
export const ModalOpen = (title, body, onClose, onAction) => {
    return {
        type: 'OPEN_MODAL',
        title,
        body,
        onClose,
        onAction,
        isRequest : true
    }
}
export const ModalClose = () => {
    return {
        type: 'CLOSE_MODAL',
        isRequest : false

    }
}