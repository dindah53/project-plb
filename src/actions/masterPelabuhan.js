import {  
    GET_PELABUHAN,
    GET_LIST_NEGARA
    } from '../constants/Constants';
import { RequestHandler, RequestHandlerDelete } from '../../src/helpers/RequestHandler';
import ENVIRONMENT from '../../src/environments/environment';

export const successGetPelabuhan = (data) => {
    return {
        type: GET_PELABUHAN,
        payload: data
    }
}

export const successGetNegara = (data) => {
    return {
        type: GET_LIST_NEGARA,
        payload: data
    }
}

export const getListPelabuhan = async (page, perpage) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftarpelabuhan/GetDaftarPelabuhan?page=${page}&perPage=${perpage}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};

export const savePelabuhan = async (valuePelabuhan) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftarpelabuhan/PostDaftarPelabuhan`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`}, valuePelabuhan);
};

export const deletePelabuhan = async (id) => {
    return await RequestHandlerDelete(`${ENVIRONMENT.API_URL}/api/v1/daftarpelabuhan/DeleteDaftarPelabuhan?Id=${id}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`});
};

export const getListNegara = async () => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftarnegara/GetDaftarNegara`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};