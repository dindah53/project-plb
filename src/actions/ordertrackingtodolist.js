import { INVALIDATE_ORDERTRACKING_TDL, 
    REQUEST_ORDERTRACKING_TDL, 
    SUCCESS_ORDERTRACKING_TDL, 
    FAILED_ORDERTRACKING_TDL, 
    FAILED_USERINFO, 
    UNAUTHORIZED,
    ListAttribute } from '../constants/Constants';
import ENVIRONMENT from '../environments/environment';
import { RequestHandler, RequestHandlerAttribute } from '../helpers'

export const invalidateOrdertrackingtodolist = () => {
    return {
        type: INVALIDATE_ORDERTRACKING_TDL
    }
}

export const successGetListATR = (data) => {
    return {
        type: ListAttribute,
        data: data
    }
}

export const requestOrdertrackingtodolist = () => {
    return {
        type: REQUEST_ORDERTRACKING_TDL
    }
}

export const successOrdertrackingtodolist = (data) => {
    return {
        type: SUCCESS_ORDERTRACKING_TDL,
        data: data
    }
}

export const failedOrdertrackingtodolist = (data) => {
    return {
        type: FAILED_ORDERTRACKING_TDL,
        data: data
    }
}

export const failedUserinfo = (data) => {
    return {
        type: FAILED_USERINFO,
        data: data
    }
}

export const unauthorized = () => {
    return {
        type: UNAUTHORIZED
    }
}

export const fetchingOrdertrackingtodolist = (accessToken, remarks, roles, page = 1, perPage = 10, filters = { search: '', plant: '', brand: '' }) => {
    return async dispatch => {
        try {
            var response = await RequestHandler(`${ENVIRONMENT.API_URL_SERVICE_OT}/CC/OT/api/v1.0/ordertracking?Remarks=${remarks}&Roles=${roles}&page=${page}&perPage=${perPage}&search=${filters.search}&plant=${filters.plant}&brand=${filters.brand}`, `${ENVIRONMENT.X_IBM_KEY_UM}`);

            dispatch(successOrdertrackingtodolist(response.data))
        } catch (err) {
            if (err.response) {
                if (err.response.status === 401) {
                    dispatch(unauthorized())
                } else {
                    dispatch(failedOrdertrackingtodolist(err.response.status))
                }
                
            } else {
                dispatch(failedOrdertrackingtodolist('Network Error'))
            }  
            
        }
    }
}

export const getAttribute = async (user) => {
    return await RequestHandlerAttribute(`${ENVIRONMENT.API_URL_USER_MANAGEMENT}CC/OT/api/user/attribute`, { 'x-ibm-client-id': ENVIRONMENT.X_IBM_KEY_ATTRIBUTE }, { 'name': user })
}
