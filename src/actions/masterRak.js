import {  
    GET_RAK
    } from '../constants/Constants';
import { RequestHandler, RequestHandlerDelete } from '../../src/helpers/RequestHandler';
import ENVIRONMENT from '../../src/environments/environment';

export const successGetDepo = (data) => {
    return {
        type: GET_RAK,
        payload: data
    }
}

export const getListRak = async (page, perpage) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftarrak/GetDaftarRak?page=${page}&perPage=${perpage}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}` });
};

export const saveRak = async (valueRak) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL}/api/v1/daftarrak/PostDaftarRak`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`}, valueRak);
};

export const deleteRak = async (id) => {
    return await RequestHandlerDelete(`${ENVIRONMENT.API_URL}/api/v1/daftarrak/DeleteDaftarRak?Id=${id}`, { 'x-ibm-client-id': `${ENVIRONMENT.IBM_KEYOT}`});
};