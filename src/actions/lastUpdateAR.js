import {
    AR_REQLASTUPDATE,
    AR_SUCCESSLASTUPDATE,
    AR_FAILEDLASTUPDATE
} from '../constants/Constants';

export const requestLastupdateAR = () => {
    return {
        type: AR_REQLASTUPDATE
    }
}

export const successLastupdateAR = (payload) => {
    return {
        type: AR_SUCCESSLASTUPDATE,
        payload,
    }
}

export const failedLastupdateAR = (message) => {
    return {
        type: AR_FAILEDLASTUPDATE,
        message
    }
}