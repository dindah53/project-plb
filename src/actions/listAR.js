import { 
    AR_REQ, 
    AR_REQCARD, 
    AR_SUCCESS, 
    AR_SUCCESSCARD, 
    AR_FAILED, 
    AR_FAILEDCARD 
} from '../constants/Constants';

export const requestAr = () => {
    return {
        type: AR_REQ
    }
}

export const requestArCard = () => {
    return {
        type: AR_REQCARD
    }
}

export const successAr = (data) => {
    return {
        type: AR_SUCCESS,
        payload: data
    }
}

export const successArCard = (data) => {
    return {
        type: AR_SUCCESSCARD,
        payload: data
    }
}

export const failedAr = (data) => {
    return {
        type: AR_FAILED,
        data: data
    }
}

export const failedArCard = (data) => {
    return {
        type: AR_FAILEDCARD,
        data: data
    }
}
