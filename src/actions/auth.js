import { RequestHandler } from '../helpers';
import ENVIRONMENT from '../environments/environment';
import { 
    USER_AUTH_REQUEST, 
    USER_AUTH_SUCCESS, 
    USER_AUTH_INVALIDATE, 
    USER_AUTH_FAILED } from '../constants/Constants';

require('dotenv').config()

export const requestAuth = User => {
    return {
        type: USER_AUTH_REQUEST,
        User
    }
}
export const successAuth = (User, data) => {
    return {
        type: USER_AUTH_SUCCESS,
        User,
        resp: data
    }
}

export const failedAuth = (User, data) => {
    return {
        type: USER_AUTH_FAILED,
        User,
        error: true,
        message: data
    }
}

export const invalidate = User => {
    return {
        type: USER_AUTH_INVALIDATE,
        User
    }
}

export const login = async (user) => {
    return await RequestHandler(`${ENVIRONMENT.API_URL_USER_MANAGEMENT}api/cc/login`, { 'x-ibm-client-id': ENVIRONMENT.X_IBM_KEY_UM }, user);
}
