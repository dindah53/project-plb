import React, { Component } from 'react';
import { 
    Button, 
    Col,  
    Form, 
    Input,
    Row,
    FormGroup, InputGroup
 } from 'reactstrap';
import { connect } from 'react-redux';
import './style-rak.css';
import ModalInput from '../../../../../src/components/Modal/index';
import DataTable from '../../../../components/DataTable/index';
import { SaveDepo, DeleteData, EditData, PageChange } from '../../../../constants/Constants';
import { FieldRak } from './FieldRak';
import { getListRak, successGetDepo, saveRak, deleteRak } from '../../../../actions/masterRak';
import swal from 'sweetalert';
import moment from 'moment';

require('dotenv').config()

class MasterDaftarRak extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            status: '',
            id: '',
            idRak: '',
            namaRak: '',
            ketRak: '',
            page: '1',
            perPage: '5'
        }
    }
    componentWillMount = async () => {
        await this._getListRak()
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal
      })
    }
  
    _onclickSave = () => {
      this.toggle()
    }

    _returnButton = () => {
        return (
            <Button className="btn btn-block btn-success" onClick={this._onclickSave} style={{ fontSize: '12px', padding: '8px 15px 8px 15px', marginLeft: 10 }} type='button'>Tambah Rak</Button>
        )
    }

    _viewForm = () => {
        return (
            <FormGroup>
                <InputGroup>
                    <label className="col-md-3 col-form-label">Kode Rak</label>
                    <Input required type="text" onChange={this._changeID} name="IDRak" placeholder="ID Rak" />
                </InputGroup>
                <InputGroup>
                    <label className="col-md-3 col-form-label">Nama Rak</label>
                    <Input required type="text" onChange={this._changeNama} name="nameRak" placeholder="Nama Rak" />
                </InputGroup>
                <InputGroup>
                    <label className="col-md-3 col-form-label">Keterangan</label>
                    <Input required type="text" onChange={this._changeKet} name="keterangan" placeholder="Keterangan" />
                </InputGroup>
            </FormGroup>
        )
    }

    _changeID = (evt) => {
        this.setState({
            idRak: evt.target.value
        })
    }

    _changeNama = (evt) => {
        this.setState({
            namaRak: evt.target.value
        })
    }

    _changeKet = (evt) => {
        this.setState({
            ketRak: evt.target.value
        })
    }

    _batalHapus = () => {
        this.setState({
            status: ''
        })
        this.toggle()
    }

    _getListRak = async () => {
        const { dispatch } = this.props;
        const { page, perPage } = this.state;
        try {
            let response = await getListRak(page, perPage);
            dispatch(successGetDepo(response.data))
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    _handleSave = async () => {
        const { idRak, namaRak, ketRak } = this.state;
        let valueRak = {
            kode_rak: idRak,
            nama: namaRak,
            keterangan: ketRak
        }
        try {
            let response = await saveRak(valueRak)
            await this.toggle();
            await this._getListRak();
            if (response.status === 200) {
                swal('Data Berhasil Disimpan!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            swal('Maaf Data Anda Gagal Tersimpan!', {
                icon: 'error',
                buttons: 'Terima Kasih'
            });
        }
    }

    _handleDelete = async () => {
        const { id } = this.state;
        try {
            let response = await deleteRak(id);
            await this.toggle();
            await this._getListRak();
            if (response.status === 200) {
                swal('Data Berhasil Di Hapus!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }

    }

    render () {
        const { status, page, perPage } = this.state;
        const { listRak } = this.props;
        let count = listRak ? listRak.totalCount : null;
        
        return (
            <div className="animated fadeIn ">
                <Row className="pt-4 containersBody">
                    <Col sm="6">
                        <ModalInput isOpen={this.state.modal} toggle={this.toggle} viewPage={this._viewForm()} status={status} name='Hapus Rak?' batalHapus={this._batalHapus}
                            OnModalChanged={async (state, data) => {
                                switch (state) {
                                    case SaveDepo:
                                        await this._handleSave();
                                        break;
                                    case DeleteData:
                                        await this._handleDelete();
                                        break;
                                    default:
                                }
                            }}
                        />
                        <DataTable 
                            button={this._returnButton()}
                            count={count}
                            page={page}
                            perPage={perPage}
                            fields={FieldRak()}
                            data={listRak.data || []}
                            jenis='rak'
                            OnTableFunctionChanged={async (state, tableData) => {
                            switch (state) {
                                case SaveDepo:
                                    this.setState({ status: '' })
                                    await this._onclickSave;
                                    break;
                                case DeleteData:
                                    this.setState({ status: 'hapus', id: tableData })
                                    this.toggle()
                                    break;
                                case EditData:
                                    this.setState({ status: '' })
                                    this.toggle()
                                    break;
                                case PageChange:
                                    this.setState({
                                        page: tableData.page
                                    }, async () => {
                                        await this._getListRak();
                                    });
                                    break;
                                default:
                            }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps ({ MasterRakReducer }) {
    return MasterRakReducer;
}

export default connect(mapStateToProps)(MasterDaftarRak);
