import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    Form, 
    Input, 
    Row,
    FormGroup,
    InputGroup
 } from 'reactstrap';
import { connect } from 'react-redux';
import './style-depo.css';
import ModalInput from '../../../../../src/components/Modal/index';
import DataTable from '../../../../components/DataTable/index';
import { SaveDepo, DeleteData, EditData, PerpageChanged, PageChange } from '../../../../constants/Constants';
import { FieldDepo } from './FieldDepo';
import { successGetDepo, failedGetDepo, getListDepo, saveDepo, deleteDepo } from '../../../../actions/masterDepo';
import swal from 'sweetalert';
import moment from 'moment';

require('dotenv').config()

class MasterDaftarDepo extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            status: '',
            nameDepo: '',
            page: '1',
            perPage: '2'
        }
    }

    componentWillMount = async () => {
        await this._getListDepo(); 
    }

    _getListDepo = async () => {
    // Untuk Mendapatkan list data depo dari database
        const { dispatch } = this.props;
        const { page, perPage } = this.state;
        try {
            let response = await getListDepo(page, perPage)
            dispatch(successGetDepo(response.data)) 
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            dispatch(failedGetDepo(error))
        }
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal
      })
    }
  
    _onclickSave = async () => {
      this.toggle()
    }

    _onClick = (e) => {
        this.setState({
            status: e,
            modal: !this.state.modal
        })
    }

    _nameDepo = async (evt) => {
        this.setState({
            nameDepo: evt.target.value
        })
    }

    _handleSave = async () => {
        const { nameDepo } = this.state;
        let daftarDepo = {
            nama: nameDepo
        }
        try {
            let response = await saveDepo(daftarDepo);
            await this.toggle();
            await this._getListDepo();
            if (response.status === 200) {
                swal('Data Berhasil Disimpan!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            swal('Maaf Data Anda Gagal Tersimpan!', {
                icon: 'error',
                buttons: 'Terima Kasih'
            });
        }
    }

    _viewForm = () => {
        return (
            <FormGroup>
                <InputGroup>
                    <label className="col-md-3 col-form-label">Nama Depo</label>
                    <Input required onChange={this._nameDepo} type="text" name="depo" placeholder="Nama Depo" />
                </InputGroup>
            </FormGroup>
        )
    }

    _returnButton = () => {
        return (
            <Button className="btn btn-block btn-success" onClick={() => this._onclickSave()} style={{ fontSize: '12px', padding: '8px 15px 8px 15px', marginLeft: 10 }} type='button'>Tambah Depo</Button>
        )
    }

    _batalHapus = () => {
        this.setState({
            status: ''
        })
        this.toggle()
    }

    _handleDelete = async () => {
        const { id } = this.state;
        try {
            let response = await deleteDepo(id);
            await this.toggle();
            await this._getListDepo();
            if (response.status === 200) {
                swal('Data Berhasil Di Hapus!', {
                    icon: 'success',
                    showCloseButton: true
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            swal('Maaf Data Anda Gagal Terhapus!', {
                icon: 'error',
                showCloseButton: true
            });
        }
    }

    render () {
        const { status, page, perPage } = this.state
        const { listDepo, message } = this.props;
        let count = listDepo ? listDepo.totalCount : null;
        
        return (
            <div>
                <Row className="pt-4 containersBody">
                    <Col sm="6">
                        <ModalInput isOpen={this.state.modal} toggle={this.toggle} viewPage={this._viewForm()} status={status} name='Hapus Depo?' batalHapus={this._batalHapus} 
                            OnModalChanged={async (state, data) => {
                                switch (state) {
                                    case SaveDepo:
                                        await this._handleSave();
                                        break;
                                    case DeleteData:
                                        await this._handleDelete();
                                        break;
                                    default:
                                }
                            }}
                        />
                        <DataTable 
                            button={this._returnButton()}
                            count={count}
                            page={page}
                            perPage={perPage}
                            data={listDepo ? listDepo.data : []}
                            fields={FieldDepo()}
                            jenis='depo'
                            OnTableFunctionChanged={async (state, tableData) => {
                                const { dispatch } = this.props;
                                switch (state) {
                                    case SaveDepo:
                                        this.setState({ status: '' })
                                        await this._onclickSave;
                                        break;
                                    case DeleteData:
                                        this.setState({ status: 'hapus', id: tableData })
                                        this.toggle()
                                        break;
                                    case EditData:
                                        this.setState({ status: '' })
                                        this.toggle()
                                        break;
                                    case PageChange:
                                        this.setState({ 
                                            page: tableData.page 
                                        }, async () => {
                                            await this._getListDepo();
                                        })
                                        break;
                                    default:
                                }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps ({ MasterReducer }) {
    return MasterReducer;
}

export default connect(mapStateToProps)(MasterDaftarDepo);
