import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    Form, 
    Input,
    Row,
    FormGroup,
    InputGroup
 } from 'reactstrap';
import { connect } from 'react-redux';
import './style-rak.css';
import ModalInput from '../../../../../src/components/Modal/index';
import DataTable from '../../../../components/DataTable/index';
import { SaveDepo, DeleteData, EditData, PageChange } from '../../../../constants/Constants';
import { FieldPerusahaan } from './FieldPerusahaan';
import { savePerushaan, getListPerusahaan, deletePerusahaan, successGetPerusahaan } from '../../../../actions/masterPerusahaan';
import swal from 'sweetalert';
import moment from 'moment';

require('dotenv').config()

class MasterDaftarPerusahaan extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            status: '',
            id: '',
            kode: '',
            alamat: '',
            nama: '',
            page: '1',
            perPage: '5'
        }
    }

    componentWillMount = async () => {
        await this._getListPerusahaan();
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal
      })
    }
  
    _onclickSave = () => {
      this.toggle()
    }

    _returnButton = () => {
        return (
            <Button className="btn btn-block btn-success" onClick={this._onclickSave} style={{ fontSize: '12px', padding: '8px 15px 8px 15px', marginLeft: 10 }} type='button'>Tambah Perusahaan</Button>
        )
    }

    _viewForm = () => {
        return (
            <Form action="" onSubmit={this.handleSubmit}>
                <FormGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Kode Perusahaan</label>
                    <Input required type="text"onChange={this._handleKd} name="kodePerusahaan" placeholder="Kode Perusahaan" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Nama Perusahaan</label>
                    <Input required type="text" onChange={this._handleNama} name="namePerusahaan" placeholder="Nama Perusahaan" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Alamat</label>
                    <Input required type="text" onChange={this._handleAlamat} name="alamat" placeholder="Alamat" />
                    </InputGroup>
                </FormGroup>
            </Form>
        )
    }

    _handleKd = (e) => {
        this.setState({
            kode: e.target.value
        })
    }

    _handleNama = (e) => {
        this.setState({
            nama: e.target.value
        })
    }

    _handleAlamat = (e) => {
        this.setState({
            alamat: e.target.value
        })
    }

    _batalHapus = () => {
        this.setState({
            status: ''
        })
        this.toggle()
    }

    _getListPerusahaan = async () => {
        const { dispatch } = this.props;
        const { page, perPage } = this.state;
        try {
            let response = await getListPerusahaan(page, perPage);
            dispatch(successGetPerusahaan(response.data))
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    _handleSave = async () => {
        const { kode, nama, alamat } = this.state;
        let valuePerusahaan = {
            kode_Perusahaan: kode,
            nama_Perusahaan: nama,
            alamat: alamat
        }
        try {
            let response = await savePerushaan(valuePerusahaan);
            await this._getListPerusahaan();
            if (response.status === 200) {
                swal('Data Berhasil Disimpan!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            swal('Maaf Data Anda Gagal Tersimpan!', {
                icon: 'error',
                buttons: 'Terima Kasih'
            });
        }
    }

    _handleDelete = async () => {
        const { id } = this.state;
        try {
            let response = await deletePerusahaan(id);
            await this._getListPerusahaan();
            if (response.status === 200) {
                swal('Data Berhasil Di Hapus!', {
                    icon: 'success',
                    buttons: 'OK'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    render () {
        const { status } = this.state
        const { listPerusahaan } = this.props;
        let count = 1;
        let page = 1;
        let perPage = 5;
        
        return (
            <div className="animated fadeIn ">
                <Row className="pt-4 containersBody">
                    <Col sm="12">
                        <ModalInput isOpen={this.state.modal} toggle={this.toggle} viewPage={this._viewForm()} status={status} name='Hapus Perusahaan?' batalHapus={this._batalHapus} 
                        OnModalChanged={async (state, data) => {
                            switch (state) {
                                case SaveDepo:
                                    await this.toggle();
                                    await this._handleSave();
                                    break;
                                case DeleteData:
                                    await this.toggle();
                                    await this._handleDelete();
                                    break;
                                default:
                            }
                        }}
                        />
                        <DataTable 
                            button={this._returnButton()}
                            count={count}
                            page={page}
                            perPage={perPage}
                            fields={FieldPerusahaan()}
                            jenis='perusahaan'
                            data={listPerusahaan.data || []}
                            OnTableFunctionChanged={async (state, tableData) => {
                            switch (state) {
                                case SaveDepo:
                                    this.setState({ status: '' })
                                    await this._onclickSave;
                                    break;
                                case DeleteData:
                                    this.setState({ status: 'hapus', id: tableData })
                                    this.toggle()
                                    break;
                                case EditData:
                                    this.setState({ status: '' })
                                    this.toggle()
                                    break;
                                case PageChange:
                                    this.setState({
                                        page: tableData.page
                                    }, async () => {
                                        await this._getListPerusahaan();
                                    });
                                    break;
                                default:
                            }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps ({ MasterAllReducer }) {
    return MasterAllReducer;
}

export default connect(mapStateToProps)(MasterDaftarPerusahaan);
