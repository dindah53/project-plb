import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Input,
    Label, 
    Row,
    FormGroup,
    Alert, InputGroup, InputGroupAddon, InputGroupText, Card, CardBody
 } from 'reactstrap';
import { connect } from 'react-redux';
import './style-rak.css';
import ModalInput from '../../../../../src/components/Modal/index';
import DataTable from '../../../../components/DataTable/index';
import { SaveDepo, DeleteData, EditData, PageChange, SaveEdit } from '../../../../constants/Constants';
import { FieldShippingline } from './FieldShippingline';
import { getListShippingline, successGetShippingline, saveShippingline, deleteShippingline } from '../../../../actions/masterShip';
import swal from 'sweetalert';
import moment from 'moment';

require('dotenv').config()

class MasterDaftarShippingline extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            status: '',
            page: '1',
            perPage: '5',
            shippingline: '',
            kapal: '',
            email: '',
            ccEmail: '',
            keterangan: ''
        }
    }

    componentWillMount = async () => {
        await this._getListShip();
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal
      })
    }
  
    _onclickSave = () => {
      this.toggle()
    }

    _returnButton = () => {
        return (
            <Button className="btn btn-block btn-success" onClick={this._onclickSave} style={{ fontSize: '12px', padding: '8px 15px 8px 15px', marginLeft: 10 }} type='button'>Tambah Shippingline</Button>
        )
    }

    _viewForm = () => {
        return (
            <Form action="" onSubmit={this.handleSubmit}>
                <FormGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Shippingline</label>
                    <Input required type="text" onChange={this._handleShip} style={{ marginBottom: 8 }} name="shippingline" placeholder="Shippingline" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Kapal</label>
                    <Input required type="text" onChange={this._handleKapal} style={{ marginBottom: 8 }} name="kapal" placeholder="Kapal" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Email</label>
                    <Input required type="text" onChange={this._handleEmail} type='email' autoComplete='email' style={{ marginBottom: 8 }} name="email" placeholder="Email" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">CC Email</label>
                    <Input required type="text" onChange={this._handleCC} type='email' autoComplete='email' style={{ marginBottom: 8 }} name="ccemail" placeholder="CC Email" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Keterangan</label>
                    <textarea required type="text" onChange={this._handleKet} className="form-control" name="keterangan" placeholder="Keterangan" style={{ width: 349 }} />
                    </InputGroup>
                </FormGroup>
            </Form>
        )
    }
    
    _handleShip = (e) => {
        this.setState({
            shippingline: e.target.value
        })
    }

    _handleKet = (e) => {
        this.setState({
            keterangan: e.target.value
        })
    }

    _handleKapal = (e) => {
        this.setState({
            kapal: e.target.value
        })
    }

    _handleEmail = (e) => {
        this.setState({
            email: e.target.value
        })
    }

    _handleCC = (e) => {
        this.setState({
            ccEmail: e.target.value
        })
    }

    _batalHapus = () => {
        this.setState({
            status: ''
        })
        this.toggle()
    }

    _getListShip = async () => {
        const { dispatch } = this.props;
        const { page, perPage } = this.state;
        try {
            let response = await getListShippingline(page, perPage);
            dispatch(successGetShippingline(response.data));
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    _handleSave = async () => {
        const { shippingline, email, ccEmail, kapal, keterangan } = this.state;
        let valueShip = {
            shipping_Line: shippingline,
            kapal: kapal,
            email: email,
            ccEmail: ccEmail,
            keterangan: keterangan
        }
        try {
            let response = await saveShippingline(valueShip);
            if (response.status === 200) {
                swal('Data Berhasil Disimpan!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            swal('Maaf Data Anda Gagal Tersimpan!', {
                icon: 'error',
                buttons: 'Terima Kasih'
            });
        }
    }

    _handleDelete = async () => {
        const { id } = this.state;
        try {
            let response = await deleteShippingline(id);
            await this._getListShip();
            if (response.status === 200) {
                swal('Data Berhasil Di Hapus!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }

    }

    render () {
        const { status, page, perPage } = this.state
        const { listShippingline } = this.props;
        let count = listShippingline ? listShippingline.totalCount : null;
        
        return (
            <div className="animated fadeIn ">
                <Row className="pt-4 containersBody">
                    <Col sm="12">
                        <ModalInput isOpen={this.state.modal} toggle={this.toggle} viewPage={this._viewForm()} status={status} name='Hapus Shippingline?' batalHapus={this._batalHapus} 
                            OnModalChanged={async (state, data) => {
                                switch (state) {
                                    case SaveDepo:
                                        await this.toggle();
                                        await this._handleSave();
                                        await this._getListShip();
                                        break;
                                    case DeleteData:
                                        await this.toggle();
                                        await this._handleDelete();
                                        await this._getListShip();
                                        break;
                                    default:
                                }
                            }}
                        />
                        <DataTable 
                            button={this._returnButton()}
                            count={count}
                            page={page}
                            perPage={perPage}
                            fields={FieldShippingline()}
                            jenis='ship'
                            data={listShippingline.data || []}
                            OnTableFunctionChanged={async (state, tableData) => {
                            switch (state) {
                                case SaveDepo:
                                    this.setState({ status: '' })
                                    await this._onclickSave;
                                    break;
                                case DeleteData:
                                    this.setState({ status: 'hapus', id: tableData })
                                    this.toggle()
                                    break;
                                case EditData:
                                    this.setState({ status: '' })
                                    this.toggle()
                                    break;
                                case PageChange:
                                    this.setState({
                                        page: tableData.page
                                    }, async () => {
                                        await this._getListShip();
                                    });
                                    break;
                                default:
                            }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps ({ MasterAllReducer }) {
    return MasterAllReducer;
}

export default connect(mapStateToProps)(MasterDaftarShippingline);
