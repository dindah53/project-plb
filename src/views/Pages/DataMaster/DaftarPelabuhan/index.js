import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    Form, 
    Input, 
    Row,
    FormGroup, InputGroup
 } from 'reactstrap';
import { connect } from 'react-redux';
import './style-rak.css';
import ModalInput from '../../../../../src/components/Modal/index';
import DataTable from '../../../../components/DataTable/index';
import { SaveDepo, DeleteData, EditData, PageChange } from '../../../../constants/Constants';
import { FieldPelabuhan } from './FieldPelabuhan';
import { getListPelabuhan, successGetPelabuhan, savePelabuhan, deletePelabuhan, successGetNegara, getListNegara } from '../../../../actions/masterPelabuhan';
import swal from 'sweetalert';
import moment from 'moment';

require('dotenv').config()

class MasterDaftarPelabuhan extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            status: '',
            kdNegara: '',
            kdPelabuhan: '',
            nmNegara: '',
            nmPelabuhan: '',
            jenis: '',
            id: '',
            page: '1',
            perPage: '5'
        }
    }

    componentWillMount = async () => {
        await this._getListPelabuhan();
        await this._getListNegara()
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal
      })
    }

    _getListNegara = async () => {
        const { dispatch } = this.props;
        try {
            let response = await getListNegara();
            dispatch(successGetNegara(response.data))
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    _viewForm = () => {
        const { listNegara } = this.props;
        return (
            <Form action="" onSubmit={this.handleSubmit}>
                <FormGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Kode Pelabuhan</label>
                    <Input required type="text" name="idPelabuhan" onChange={this._handleKode} placeholder="Kode Pelabuhan" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Nama Pelabuhan</label>
                    <Input required type="text" onChange={this._handleNama} name="namePLB" placeholder="Nama Pelabuhan" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label" >Jenis</label>
                    <div >
                        <select className="form-control form-control-sm" onChange={this._handleJenis} id="jenis" name="jenis">
                            <option>--- Select Option ---</option>
                            <option value='D'>D</option>
                            <option value='L'>L</option>
                        </select>
                    </div>
                    </InputGroup>
                    <InputGroup>
                        <label className="col-md-3 col-form-label" >Kode Negara</label>
                        <div >
                            <select className="form-control form-control-sm" onChange={this._handleNgr} id="jenis" name="jenis">
                                <option>--- Select Option ---</option>
                        {
                            listNegara ?
                            listNegara.map(value => {
                                return (
                                    <option value={value.kode_Negara}>{value.kode_Negara}</option>
                                )
                            }) : null
                        }
                            </select>
                        </div>
                    </InputGroup>
                    <InputGroup>
                        <label className="col-md-3 col-form-label" >Nama Negara</label>
                        <div >
                            <select className="form-control form-control-sm" onChange={this._handleNmNGR} id="jenis" name="jenis">
                                <option>--- Select Option ---</option>
                        {
                            listNegara ?
                            listNegara.map(value => {
                                return (
                                    <option value={value.nama_Negara}>{value.nama_Negara}</option>
                                )
                            }) : null
                        }
                            </select>
                        </div>
                    </InputGroup>
                </FormGroup>
            </Form>
        )
    }

    _handleNmNGR = (e) => {
        this.setState({
            nmNegara: e.target.value
        })
    }

    _handleNgr = (e) => {
        this.setState({
            kdNegara: e.target.value
        })
    }

    _handleJenis= () => {
        var e = document.getElementById('jenis');
        var strJenis = e.options[e.selectedIndex].value;
        this.setState({
            jenis: strJenis
        })
    }

    _handleKode = (e) => {
        this.setState({
            kdPelabuhan: e.target.value
        })
    }

    _handleNama = (e) => {
        this.setState({
            nmPelabuhan: e.target.value
        })
    }

    _returnButton = () => {
        return (
            <Button className="btn btn-block btn-success" onClick={() => this.toggle()} style={{ fontSize: '12px', padding: '8px 15px 8px 15px', marginLeft: 10 }} type='button'>Tambah Pelabuhan</Button>
        )
    }

    _batalHapus = () => {
        this.setState({
            status: ''
        })
        this.toggle()
    }

    _getListPelabuhan = async () => {
        const { dispatch } = this.props;
        const { page, perPage } = this.state;
        try {
            let response = await getListPelabuhan(page, perPage);
            dispatch(successGetPelabuhan(response.data))
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    _handleSave = async () => {
        const { jenis, kdPelabuhan, kdNegara, nmPelabuhan, nmNegara } = this.state;
        let valuePelabuhan = {
            kode_Pelabuhan: kdPelabuhan,
            nama: nmPelabuhan,
            jenis: jenis,
            kode_Negara: kdNegara,
            nama_Negara: nmNegara
        }
        try {
            let response = await savePelabuhan(valuePelabuhan)
            await this._getListPelabuhan();
            if (response.status === 200) {
                swal('Data Berhasil Disimpan!', {
                    icon: 'success',
                    buttons: 'Terima Kasih'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
            swal('Maaf Data Anda Gagal Tersimpan!', {
                icon: 'error',
                buttons: 'Terima Kasih'
            });
        }
    }

    _handleDelete = async () => {
        const { id } = this.state;
        try {
            let response = await deletePelabuhan(id);
            await this._getListPelabuhan();
            if (response.status === 200) {
                swal('Data Berhasil Di Hapus!', {
                    icon: 'success',
                    buttons: 'OK'
                });
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.props.history.push('/login'); 
            }
        }
    }

    render () {
        const { status, page, perPage } = this.state
        const { listPelabuhan } = this.props;
        let count = listPelabuhan ? listPelabuhan.totalCount : null;
        
        return (
            <div className="animated fadeIn ">
                <Row className="pt-4 containersBody">
                    <Col sm="12">
                        <ModalInput isOpen={this.state.modal} toggle={this.toggle} viewPage={this._viewForm()} status={status} name='Hapus Pelabuhan?' batalHapus={this._batalHapus} 
                            OnModalChanged={async (state, data) => {
                                switch (state) {
                                    case SaveDepo:
                                        await this._handleSave();
                                        await this.toggle()
                                        break;
                                    case DeleteData:
                                        await this._handleDelete();
                                        await this.toggle();
                                        break;
                                    default:
                                }
                            }}
                        />
                        <DataTable 
                            button={this._returnButton()}
                            count={count}
                            page={page}
                            perPage={perPage}
                            data={listPelabuhan.data}
                            fields={FieldPelabuhan()}
                            jenis='pelabuhan'
                            OnTableFunctionChanged={async (state, tableData) => {
                            switch (state) {
                                case SaveDepo:
                                    this.setState({ status: '' })
                                    await this.toggle();
                                    break;
                                case DeleteData:
                                    this.setState({ status: 'hapus', id: tableData })
                                    this.toggle()
                                    break;
                                case EditData:
                                    this.setState({ status: '' })
                                    this.toggle()
                                    break;
                                case PageChange:
                                    this.setState({
                                        page: tableData.page
                                    }, async () => {
                                        await this._getListPelabuhan()
                                    })
                                    break;
                                default:
                            }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps ({ MasterAllReducer }) {
    return MasterAllReducer;
}

export default connect(mapStateToProps)(MasterDaftarPelabuhan);
