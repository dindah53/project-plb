import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Input,
    Label, 
    Row,
    FormGroup,
    Alert, InputGroup, InputGroupAddon, InputGroupText
 } from 'reactstrap';
import { connect } from 'react-redux';
import Loader from './Loader';
import { login, requestAuth, successAuth, failedAuth } from '../../../actions/auth';
import './Styles.css';
import moment from 'moment';
import { Card } from 'semantic-ui-react';

require('dotenv').config()

class Login extends Component {
    constructor (props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isLogin: false,
            loginAttempt: 0,
            endFailedAttempt: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUsernameChanged = this.handleUsernameChanged.bind(this);
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this);
    }

    async handleSubmit (e) {
        e.preventDefault();
        const { dispatch } = this.props;
        const { username, password } = this.state;
        const user = {
            username: username,
            password: password
        }
        let response;
        // try {
        //     dispatch(requestAuth(user))
        //     response = await login(user);
        //     dispatch(successAuth(user, response.data));
        //     window.localStorage.setItem('user', JSON.stringify(response.data));
        //     this.props.history.push('/home');
        // } catch (error) {
        //     dispatch(failedAuth(user, error.response.data));
        //     let attemp = ++this.state.loginAttempt;
        //     this.setState({
        //         loginAttempt: attemp,
        //         username: '',
        //         password: ''
        //     }, () => {
        //         document.getElementById('formLogin').reset();
        //         if (attemp > 2) {
        //             this.setState({ endFailedAttempt: true })
        //             const endDate = moment(new Date()).add(10, 'minute');
        //             localStorage.setItem('endFailedAttempt', moment(endDate))
        //             this._countDownTimer(endDate)
    
        //         }
        //     })

        // }
        if (user !== null) {
            this.props.history.push('/home');
        }
    }

    _countDownTimer = (endDate) => {
        let countDownDate = endDate

        let x = setInterval(() => {

            let now = new Date().getTime();

            let distance = countDownDate - now;

            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            this.setState({ msgFailedAttempt: `${minutes}:${seconds}` })

            if (distance < 0) {
                clearInterval(x);
                localStorage.clear();
                this.setState({ endFailedAttempt: false, loginAttempt: 0 })
            }
        }, 1000);

    }

    componentDidMount () {
        if (localStorage.getItem('endFailedAttempt')) {
            const endFailedAttempt = localStorage.getItem('endFailedAttempt');
            const currentDate = moment(new Date());
            if (moment(endFailedAttempt) > currentDate) {
                this.setState({ endFailedAttempt: true })
                const endDate = moment(endFailedAttempt);
                this._countDownTimer(endDate)
            }
        }
    }
    
    handleUsernameChanged (e) {
        this.setState({
            username: e.target.value
        })
    }

    handlePasswordChanged (e) {
        this.setState({
            password: e.target.value
        })
    }

    _messageAlert = () => {
        const { loginAttempt, endFailedAttempt } = this.state;
        const { error, message } = this.props;
        if (error) {
            if (loginAttempt <= 2) {
                return (
                    <Alert color="danger">{message}</Alert>
                )
            } else if (endFailedAttempt === true) {
                return (
                    <div class="alert alert-danger fade show p-3" role="alert">
                        You failed 3 times, please login after <b>{this.state.msgFailedAttempt}</b>
                    </div>
                )
            }
        }
    }

    render () {
        const { isLogin } = this.props;
        return (
            <div className="app flex-row align-items-center white-background">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="12" className="zero-padding">
                            <div className="d-flex justify-content-center align-items-center" style={{ height: window.innerHeight, width: '100%' }}>
                            </div>
                        </Col>
                    </Row>
                </Container>

                <Container>
                    <Row className="justify-content-center" style={{ backgroundColor: '#FFCE07', height: window.innerHeight, width: '100%' }}>
                        <Col md="6">
                            <div className="d-flex justify-content-center">
                                <h2 className='h2Login'>Login</h2>
                            </div>
                            <Form action="" onSubmit={this.handleSubmit}>
                                <FormGroup>
                                    <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                                    </InputGroupAddon>
                                    <Input required type="text" id="username1" name="username1" placeholder="Username" autoComplete="username"/>
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                                    </InputGroupAddon>
                                    <Input required type="password" id="password1" name="password1" placeholder="Password" autoComplete="current-password"/>
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup className="form-actions">
                                    <Row>
                                        <Col xs="12">
                                            <Button color='green' className="btn btn-sm btn-success justify-content-center" style={{ width: 225, marginTop: 40, marginLeft: 57, height: 35, fontSize: 15 }} disabled={isLogin}>{isLogin ? <Loader /> : 'Login'}</Button>
                                        </Col>
                                    </Row>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

function mapStateToProps (state) {
    const { auth } = state;
    return auth;
}

export default connect(mapStateToProps)(Login);
