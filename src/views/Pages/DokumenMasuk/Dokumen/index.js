import React, { Component } from 'react';
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Input,
    Label, 
    Row,
    FormGroup,
    Alert, InputGroup, InputGroupAddon, InputGroupText, Card, CardBody, UncontrolledPopover,
    PopoverBody, PopoverHeader
 } from 'reactstrap';
import { connect } from 'react-redux';
import './style-dokumen.css';
import ModalInput from '../../../../../src/components/Modal/index';
import DataTable from '../../../../components/DataTable/index';
import { SaveDepo, DeleteData, EditData } from '../../../../constants/Constants';
import { FieldDokumen } from './FieldDokumen';
import Calender from '../../../../components/Calender/index';
import InputDoctAju from './InputDoctAju';
import InputDetailInput from './InputDetailIdentitas';
import InputDoct from './InputDoct';
import moment from 'moment';

require('dotenv').config()

class Dokumen extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            status: ''
        }
    }

    componentWillMount = () => {
        
    }

    toggle = () => {
      this.setState({
        modal: !this.state.modal
      })
    }
  
    _onclickSave = async () => {
        let data = {
            id: 'edit'
        }
        this.props.history.push({ pathname: '/dokumenmasuk-dokumen/input', data }); 
    }

    _onClick = (e) => {
        this.setState({
            status: e,
            modal: !this.state.modal
        })
    }

    _returnButton = () => {
        return (
            <Button className="btn btn-block btn-success" onClick={() => this._onclickSave()} style={{ fontSize: '12px', padding: '8px 15px 8px 15px', marginLeft: 10 }} type='button'>Tambah Dokumen</Button>
        )
    }

    _returnFilter = () => {
        return (
            <div style={{ marginBottom: 20, marginLeft: 10, display: 'flex' }}>
                <button id="PopoverArea" title="Plant" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px', marginRight: 10 }} >Pencarian Berdasarkan<i className="fa fa-chevron-down"></i></button>
                <UncontrolledPopover trigger="legacy" placement="bottom" target="PopoverArea">
                    <UncontrolledPopover trigger="legacy" placement="bottom" target="PopoverShortPeriode">
                        <PopoverHeader>Choose Week...</PopoverHeader>
                        <PopoverBody>
                            <ul className="list-group popover-dropdown">
                            </ul>
                        </PopoverBody>
                    </UncontrolledPopover>
                </UncontrolledPopover>
                <Calender className='calender' />
                s/d
                <Calender className='calenderRight' />
                <button fixed style={{ width: '120px', height: '35px', marginLeft: 10, backgroundColor: '#05dfd7' }} >Search</button>
            </div>
        )
    }

    _batalHapus = () => {
        this.setState({
            status: ''
        })
        this.toggle()
    }

    render () {
        const { status } = this.state
        let count = 1;
        let page = 1;
        let perPage = 5;
        
        return (
            <div>
                <Row className="pt-4 containersBody">
                    <Col >
                        <ModalInput isOpen={this.state.modal} toggle={this.toggle} status={status} name='Hapus Depo?' batalHapus={this._batalHapus} />
                        <DataTable 
                            button={this._returnButton()}
                            filter={this._returnFilter()}
                            count={count}
                            page={page}
                            perPage={perPage}
                            fields={FieldDokumen()}
                            OnTableFunctionChanged={async (state, tableData) => {
                                const { dispatch } = this.props;
                                switch (state) {
                                    case SaveDepo:
                                        this.setState({ status: '' })
                                        await this._onclickSave;
                                        break;
                                    case DeleteData:
                                        this.setState({ status: tableData })
                                        this.toggle()
                                        break;
                                    case EditData:
                                        this.setState({ status: '' })
                                        this.toggle()
                                        break;
                                    default:
                                }
                            }}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Dokumen;
