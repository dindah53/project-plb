import React, { Component } from 'react';
import { 
    FormGroup, 
    InputGroup, 
    Input,
    Card
} from 'reactstrap';
import { StateNext } from '../../../../constants/Constants';

class InputDetailIdentitas extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            tglTiba: '',
            penutup: '',
            nomor: '',
            tanggal: '',
            noPos: '',
            tempatTimbun: '',
            valuta: '',
            kdHarga: '',
            jnsNilai: '',
            nilai: '',
            bruto: '',
            neto: ''
        }
    }

    _handleClickNext = () => {
        const { tglTiba, penutup, nomor, tanggal, noPos, tempatTimbun, valuta, kdHarga, jnsNilai, nilai, bruto, neto } = this.state;
        const { next, onChange } = this.props;
        let data = {
            tglTiba: tglTiba,
            penutup: penutup,
            nomor: nomor,
            tanggal: tanggal,
            noPos: noPos,
            tempatTimbun: tempatTimbun,
            valuta: valuta,
            kdHarga: kdHarga,
            jnsNilai: jnsNilai,
            nilai: nilai,
            bruto: bruto,
            neto: neto
        }
        next();
        onChange(StateNext, data);
    }

    _handleTglTiba = (e) => {
        this.setState({
            tglTiba: e.target.value
        })
    }

    _handlePenutup = (e) => {
        this.setState({
            penutup: e.target.value
        })
    }

    _handleNomor = (e) => {
        this.setState({
            nomor: e.target.value
        })
    }

    _handleTgl = (e) => {
        this.setState({
            tanggal: e.target.value
        })
    }

    _handleNoPos = (e) => {
        this.setState({
            noPos: e.target.value
        })
    }

    _handleTimbun = (e) => {
        this.setState({
            tempatTimbun: e.target.value
        })
    }

    _handleValuta = (e) => {
        this.setState({
            valuta: e.target.value
        })
    }

    _handleKdHarga = (e) => {
        this.setState({
            kdHarga: e.target.value
        })
    }

    _handleJnsNilai = (e) => {
        this.setState({
            jnsNilai: e.target.value
        })
    }

    _handleNilai = (e) => {
        this.setState({
            nilai: e.target.value
        })
    }

    _handleBruto = (e) => {
        this.setState({
            bruto: e.target.value
        })
    }

    _handleNeto = (e) => {
        this.setState({
            neto: e.target.value
        })
    }

    render () {
        const { previous } = this.props;
        
        return (
            <Card style={{ width: '75%', padding: 15 }}>
                <FormGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Perkiraan Tanggal Tiba</label>
                    <Input required type="text" onChange={this._handleTglTiba} style={{ marginBottom: 8 }} name="PerkiraanTglTiba" placeholder="Perkiraan Tanggal Tiba" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Penutup</label>
                    <Input required type="text" onChange={this._handlePenutup} style={{ marginBottom: 8 }} name="Penutup" placeholder="Penutup" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Nomor</label>
                    <Input required type="text" onChange={this._handleNomor} type='number' style={{ marginBottom: 8 }} name="Nomor" placeholder="Nomor" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Tanggal</label>
                    <Input required type="text" onChange={this._handleTgl} style={{ marginBottom: 8 }} name="Tanggal" placeholder="Tanggal" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Nomor Pos</label>
                    <Input required type="text" onChange={this._handleNoPos} type='number' style={{ marginBottom: 8 }} name="noPos" placeholder="Nomor Pos" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Tempat Timbun</label>
                    <Input required type="text" onChange={this._handleTimbun} style={{ marginBottom: 8 }} name="tempatTimbun" placeholder="Tempat Timbun" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Valuta</label>
                    <Input required type="text" onChange={this._handleValuta} style={{ marginBottom: 8 }} name="Valuta" placeholder="Valuta" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Kode Harga</label>
                    <Input required type="text" onChange={this._handleKdHarga} style={{ marginBottom: 8 }} name="kdHarga" placeholder="Kode Harga" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Jenis Nilai</label>
                    <Input required type="text" onChange={this._handleJnsNilai} style={{ marginBottom: 8 }} name="jnsNilai" placeholder="Jenis Nilai" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Nilai</label>
                    <Input required type="text" onChange={this._handleNilai} style={{ marginBottom: 8 }} name="nilai" placeholder="Nilai" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Bruto</label>
                    <Input required type="text" onChange={this._handleBruto} style={{ marginBottom: 8 }} name="bruto" placeholder="Bruto" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Neto</label>
                    <Input required type="text" onChange={this._handleNeto} style={{ marginBottom: 8 }} name="neto" placeholder="Neto" />
                    </InputGroup>
                    <div style={{ display: 'flex' }}>
                        <button onClick={ previous } className="btn btn-block btn-success" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px', marginRight: 15, marginTop: 8 }} >Previous</button>
                        <button onClick={ () => this._handleClickNext() } className="btn btn-block btn-warning" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px' }} >Next</button>
                    </div>
                </FormGroup>
            </Card>
        )
    }
}

export default InputDetailIdentitas;