import React, { Component } from 'react';
import { 
    FormGroup, 
    InputGroup, 
    Input,
    Card
} from 'reactstrap';
import { StateSubmit } from '../../../../constants/Constants';

class InputDoct extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            nmTtd: '',
            jabatan: '',
            tempat: '',
            tanggal: '',
            uploadDtBarang: '',
            uploadDtKemasan: '',
            uploadDtKontainer: '',
            uploadDtDokumen: ''
        }
    }

    _handleNmTtd = (e) => {
        this.setState({
            nmTtd: e.target.value
        })
    }

    _handleJabatan = (e) => {
        this.setState({
            jabatan: e.target.value
        })
    }

    _handleTempat = (e) => {
        this.setState({
            tempat: e.target.value
        })
    }

    _handleTgl = (e) => {
        this.setState({
            tanggal: e.target.value
        })
    }

    _handleUploadDtBarang = (e) => {
        this.setState({
            uploadDtBarang: e.target.value
        })
    }

    _handleUploadDtKemasan = (e) => {
        this.setState({
            uploadDtKemasan: e.target.value
        })
    }

    _handleDtKontainer = (e) => {
        this.setState({
            uploadDtKontainer: e.target.value
        })
    }

    _handleDtDokumen = (e) => {
        this.setState({
            uploadDtDokumen: e.target.value
        })
    }

    _handleSubmit = () => {
        const { nmTtd, jabatan, tempat, tanggal, uploadDtBarang, uploadDtKemasan, uploadDtKontainer, uploadDtDokumen } = this.state;
        const { submit, onChange } = this.props;
        let data = {
            nmTtd: nmTtd,
            jabatan: jabatan,
            tempat: tempat,
            tanggal: tanggal,
            uploadDtBarang: uploadDtBarang,
            uploadDtKemasan: uploadDtKemasan,
            uploadDtKontainer: uploadDtKontainer,
            uploadDtDokumen: uploadDtDokumen
        }
        submit();
        onChange(StateSubmit, data)

    }

    render () {
        const { previous } = this.props;
        
        return (
            <Card style={{ width: '75%', padding: 15 }}>
                <FormGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Nama Penandatangan Dokumen</label>
                    <Input required type="text" onChange={this._handleNmTtd} style={{ marginBottom: 8 }} name="nmTtdDokumen" placeholder="Nama Penandatangan Dokumen" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Jabatan</label>
                    <Input required type="text" onChange={this._handleJabatan} style={{ marginBottom: 8 }} name="jabatan" placeholder="Jabatan" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Tempat</label>
                    <Input required type="text" onChange={this._handleTempat} style={{ marginBottom: 8 }} name="tempat" placeholder="Tempat" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Tanggal</label>
                    <Input required type="text" onChange={this._handleTgl} style={{ marginBottom: 8 }} name="tanggal" placeholder="Tanggal" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Upload Data Barang</label>
                    <Input required type="text" onChange={this._handleUploadDtBarang} style={{ marginBottom: 8 }} name="uploadDataBrang" placeholder="Upload Data Barang" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Upload Data Kemasan</label>
                    <Input required type="text" onChange={this._handleUploadDtKemasan} style={{ marginBottom: 8 }} name="uploadDataKemasan" placeholder="Upload Data Kemasan" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Upload Data Kontainer</label>
                    <Input required type="text" onChange={this._handleDtKontainer} style={{ marginBottom: 8 }} name="uploadDtKontainer" placeholder="Upload Data Kontainer" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Upload Data Dokumen</label>
                    <Input required type="text" onChange={this._handleDtDokumen} style={{ marginBottom: 8 }} name="uploadDtDokumen" placeholder="Upload Data Dokumen" />
                    </InputGroup>
                    <div style={{ display: 'flex' }}>
                        <button onClick={ previous } className="btn btn-block btn-success" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px', marginRight: 15, marginTop: 8 }} >Previous</button>
                        <button onClick={ () => this._handleSubmit() } className="btn btn-block btn-warning" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px' }} >Submit</button>
                    </div>
                </FormGroup>
            </Card>
        )
    }
}

export default InputDoct;