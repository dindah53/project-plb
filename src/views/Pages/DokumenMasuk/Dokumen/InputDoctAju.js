import React, { Component } from 'react';
import { 
    FormGroup, 
    InputGroup, 
    Input,
    Card
} from 'reactstrap';
import { StateNext } from '../../../../constants/Constants';

class InputDoctAju extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false,
            noAju: '',
            tglAju: '',
            kpbcBongkar: '',
            kpbcPengawas: '',
            noPendaftaran: '',
            pengirim: '',
            penjual: '',
            pemilik: '',
            saranaAngkut: '',
            pelabuhan: '',
            tglPendaftaran: ''
        }
    }

    _handleClickNext = () => {
        const { noAju, tglAju, kpbcBongkar, kpbcPengawas, noPendaftaran, tglPendaftaran, pengirim, penjual, pemilik, saranaAngkut, pelabuhan } = this.state;
        const { next, onChange } = this.props;
        let data = {
            noAju: noAju,
            tglAju: tglAju,
            kpbcBongkar: kpbcBongkar,
            kpbcPengawas: kpbcPengawas,
            noPendaftaran: noPendaftaran,
            tglPendaftaran: tglPendaftaran,
            pengirim: pengirim,
            penjual: penjual,
            pemilik: pemilik,
            saranaAngkut: saranaAngkut,
            pelabuhan: pelabuhan
        }
        next();
        onChange(StateNext, data);
    }

    _handleNoAju = (e) => {
        this.setState({
            noAju: e.target.value
        })
    }

    _handleTglAju = (e) => {
        this.setState({
            tglAju: e.target.value
        })
    }

    _handleBongkar = (e) => {
        this.setState({
            kpbcBongkar: e.target.value
        })
    }

    _handlePengawas = (e) => {
        this.setState({
            kpbcPengawas: e.target.value
        })
    }

    _handleNoPendaft = (e) => {
        this.setState({
            noPendaftaran: e.target.value
        })
    }

    _handleTglPendaft = (e) => {
        this.setState({
            tglPendaftaran: e.target.value
        })
    }

    _handlePengirim = (e) => {
        this.setState({
            pengirim: e.target.value
        })
    }

    _handlePenjual = (e) => {
        this.setState({
            penjual: e.target.value
        })
    }

    _handlePemilik = (e) => {
        this.setState({
            pemilik: e.target.value
        })
    }

    _handleSarana = (e) => {
        this.setState({
            saranaAngkut: e.target.value
        })
    }

    _handlePelabuhan = (e) => {
        this.setState({
            pelabuhan: e.target.value
        })
    }

    render () {
        const { batal } = this.props;
        
        return (
            <Card style={{ width: '75%', padding: 15 }}>
                <FormGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">No Aju</label>
                    <Input required type="text" onChange={this._handleNoAju} style={{ marginBottom: 8 }} name="NoAju" placeholder="No Aju" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Tanggal Aju</label>
                    <Input required type="text" onChange={this._handleTglAju} style={{ marginBottom: 8 }} name="TglAju" placeholder="Tanggal Aju" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">KPBC Bongkar</label>
                    <Input required type="text" onChange={this._handleBongkar} type='text' style={{ marginBottom: 8 }} name="Bongkar" placeholder="KPBC Bongkar" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">KPBC Pengawas</label>
                    <Input required type="text" onChange={this._handlePengawas} type='text' style={{ marginBottom: 8 }} name="Pengawas" placeholder="KPBC Pengawas" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">No Pendaftaran</label>
                    <Input required type="text" onChange={this._handleNoPendaft} type='text' style={{ marginBottom: 8 }} name="NoPendaftr" placeholder="No Pendaftaran" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Tanggal Pendaftaran</label>
                    <Input required type="text" onChange={this._handleTglPendaft} type='text' style={{ marginBottom: 8 }} name="tglPendaftr" placeholder="Tanggal Pendaftaran" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Pengirim</label>
                    <Input required type="text" onChange={this._handlePengirim} type='text' style={{ marginBottom: 8 }} name="Pengirim" placeholder="Pengirim" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Penjual</label>
                    <Input required type="text" onChange={this._handlePenjual} type='text' style={{ marginBottom: 8 }} name="Penjual" placeholder="Penjual" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Pemilik</label>
                    <Input required type="text" onChange={this._handlePemilik} type='text' style={{ marginBottom: 8 }} name="Pemilik" placeholder="Pemilik" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Sarana Angkut</label>
                    <Input required type="text" onChange={this._handleSarana} type='text' style={{ marginBottom: 8 }} name="SaranaAngkut" placeholder="Sarana Angkut" />
                    </InputGroup>
                    <InputGroup>
                    <label className="col-md-3 col-form-label">Pelabuhan</label>
                    <Input required type="text" onChange={this._handlePelabuhan} type='text' style={{ marginBottom: 8 }} name="Pelabuhan" placeholder="Pelabuhan" />
                    </InputGroup>
                    <div style={{ display: 'flex' }}>
                        <button onClick={ () => this._handleClickNext() } className="btn btn-block btn-success" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px', marginRight: 15, marginTop: 8 }} >Next</button>
                        <button onClick={ batal } className="btn btn-block btn-warning" style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden', width: '145px', height: '35px' }} >Batal</button>
                    </div>
                </FormGroup>
            </Card>
        )
    }
}

export default InputDoctAju;