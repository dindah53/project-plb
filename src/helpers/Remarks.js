export const Remarks = (roles) => {
    if (roles === 'COP')
        return [
            'SO OPEN',
            'PENDING TO',
            'PENDING GI CUST',
            'PENDING INVOICE'
        ]
    
    if (roles === 'PDC')
        return [
            'PENDING TOC',
            'PENDING BAST',
            'RSPD PENDING GI',
            'RSPD PENDING GR',
            'EO PENDING GR'
        ]
    
    if (roles === 'PROCUREMENT')
        return [
            'FOLLOWUP INDENT EO'
        ]
    if (roles !== 'ADMINCC')
        return [
            'SO OPEN',
            'PENDING TO',
            'PENDING TOC',
            'PENDING GI CUST',
            'PENDING INVOICE',
            'PENDING BAST',
            'RSPD PENDING GI',
            'RSPD PENDING GR',
            'EO PENDING GR'
        ]
    return [
        'SO OPEN',
        'PENDING TO',
        'PENDING TOC',
        'PENDING GI CUST',
        'PENDING INVOICE',
        'PENDING BAST',
        'RSPD PENDING GI',
        'RSPD PENDING GR',
        'EO PENDING GR',
        'FOLLOWUP INDENT EO'
    ]
}