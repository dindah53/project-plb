import axios from 'axios';

export const ExportRequestHandler = async (url, headers = null, data = null) => {
    var method = 'GET';
    if (data != null)
        method = 'POST'

    const config = {
        url: url,
        method: method,
        headers: {
            ...headers
        },
        data: data
    }
    return await axios(config);

}

export const RequestHandler = async (url, headers = null, data = null) => {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(data)
    var method = 'GET';
    if (data != null)
        method = 'POST'

    const config = {
        url: url,
        method: method,
        headers: {
            ...headers,
            'Authorization': `Bearer ${
                user == null ?
                    null
                    :
                    user.tokenResponse.accessToken
                }`
        },
        data: data
    }
    return await axios(config);

}

export const RequestHandlerDelete = async (url, headers = null) => {
    const user = JSON.parse(localStorage.getItem('user'));
    var method = 'DELETE';

    const config = {
        url: url,
        method: method,
        headers: {
            ...headers,
            'Authorization': `Bearer ${
                user == null ?
                    null
                    :
                    user.tokenResponse.accessToken
                }`
        }
    }
    return await axios(config);

}

export const RequestHandlerAR = async (url, headers = null, data = null) => {
    const user = JSON.parse(localStorage.getItem('user'));
    var method = 'POST';

    const config = {
        url: url,
        method: method,
        headers: {
            ...headers,
            'Authorization': `Bearer ${
                user == null ?
                    null
                    :
                    user.tokenResponse.accessToken
                }`
        },
        data: data
    }
    return await axios(config);

}

export const BlobRequestHandler = async ({ url, fileName  }) => {
    return new Promise((resolve, reject) => {
        const user = JSON.parse(localStorage.getItem('user'));
        axios({
            url,
            method: 'POST',
            responseType: 'blob',
            headers: {
                'Authorization': `Bearer ${user.tokenResponse.accessToken}`,
            }
        }).then((response) => {
            console.log(`status code ${response.status}`);
            if(response && response.status === 200) {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', fileName);
                document.body.appendChild(link);
                link.click();
            }
            resolve(response);
 
        }).catch((e) => {
            reject(e);
 
        });
    });
 
 }
 
 export const RequestHandlerAttribute = async (url, headers = null, data = null) => {
    const user = JSON.parse(localStorage.getItem('user'));
    var method = 'GET';
    const config = {
        url: url,
        method: method,
        headers: {
            ...headers,
            'Authorization': `Bearer ${
                user == null ?
                    null
                    :
                    user.tokenResponse.accessToken
                }`
        },
        data: data
    }
    return await axios(config);

}