import React from 'react'
import { Input } from 'reactstrap'
import { useState } from 'react'

function TextFilter (props) {
   let unfiltered = props.data

    return (
        <div>
        {unfiltered.filter(res => res.includes(props.filterBy.toUpperCase())).map((filtered, index) => (
        <li style={{ cursor: 'pointer' }} key={index} onClick={() => props.onChange(filtered)} className="list-group-item list-group-item-action"><Input style={{ cursor: 'pointer' }} type="checkbox" checked={props.choosen == filtered}/>{filtered}</li>
      ))}
    </div>   
    )
}

export default TextFilter
