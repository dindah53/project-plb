import React, { useState } from 'react';

function Textarea ({ onChange, value, className, style, required, placeholder }) {
    let [text, setText] = useState(null);
    const onChangeHandler = (e) => {
        setText(e.target.value);
        onChange(e);
    }
    return <textarea style={style} value={text === null ? value : text} onChange={onChangeHandler} required={required} type="text" className={className} placeholder={placeholder} />
}

export const TextAreaComponent = Textarea;