import React, { Component } from 'react';
import { 
    CardBody, 
    Modal, 
    ModalBody, 
    Button, 
    ModalHeader, 
    ModalFooter 
} from 'reactstrap';
import { DeleteData, SaveDepo } from '../../constants/Constants';

class ModalInput extends Component {
    constructor (props) {
        super(props);
        this.state = {
            modal: false
        }
    }

    toggle () {
        this.setState({
            modal: !this.state.modal
        });
    }

    _handleClickSave = async () => {
        const { OnModalChanged } = this.props;
        OnModalChanged(SaveDepo);
    }

    _handleDelete = async () => {
        const { OnModalChanged } = this.props;
        OnModalChanged(DeleteData)
    }

    render () {
        const { isOpen, toggle, viewPage, status, name, batalHapus } = this.props;
        return (
            <div >
                {
                    status === 'hapus' ?
                    <Modal isOpen={isOpen} toggle={toggle} style={{ width: 225 }}>
                        <ModalBody>
                            {
                                name
                            }
                        </ModalBody>
                        <ModalFooter>
                            <Button className="btn btn-block btn-info" style={{ width: 130 }} onClick={() => { this._handleDelete() }} >Hapus</Button>
                            <Button color="primary" style={{ width: 130 }} onClick={batalHapus}>Batal</Button>
                        </ModalFooter>
                    </Modal>
                    :
                    <CardBody>
                        <Modal isOpen={isOpen} toggle={toggle}>
                            <ModalHeader toggle={toggle}></ModalHeader>
                            <ModalBody>
                                {
                                    viewPage
                                }
                            </ModalBody>
                            <ModalFooter>
                                {
                                    status === 'edit' ?
                                    <Button className="btn btn-block btn-info" style={{ width: 130 }} onClick={() => this._handleClickSave()} >Update</Button>
                                    :
                                    <Button className="btn btn-block btn-info" style={{ width: 130 }} onClick={() => this._handleClickSave()} >Simpan</Button>
                                }
                                <Button color="primary" style={{ width: 130 }} onClick={toggle}>Batal</Button>
                            </ModalFooter>
                        </Modal>
                    </CardBody>
                }
            </div>
        )
    }
}

export default ModalInput;