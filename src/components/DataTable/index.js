import React, { Component } from 'react';
import {
  Col,
  Button,
  Row
} from 'reactstrap';
import { SaveDepo, PageChange, PerpageChanged, DeleteData } from '../../constants/Constants';
import Table from './DRTable';
import { Card } from 'semantic-ui-react';

class Datatable extends Component {
  constructor (props) {
    super(props);
    this.state = {
    }
  }

  _onclickSave = () => {
    const { OnTableFunctionChanged } = this.props;
    OnTableFunctionChanged(SaveDepo)
  }

  _onClickDelete = () => {
    const { OnTableFunctionChanged } = this.props;
    OnTableFunctionChanged(DeleteData)
  }

  renderingPaging (maxPage, currentPage) {
      const { OnTableFunctionChanged } = this.props;
      var component = [];
      var cPage = 1;
      var maxPerPage = 5
      for (var x = 1; x <= currentPage; x++) {
          if (x % maxPerPage === 0) {
              if (cPage === 1) {
                  cPage += 3;
              } else {
                  cPage += 5;
              }
              
              maxPerPage += 5;
          }
      }
      for (var i = cPage; i <= maxPerPage; i++) {
          if (i <= maxPage) {
              component.push(<li onClick={(e) => {
                  this.setState({
                      ...this.state,
                      page: parseInt(e.target.id)
                  }, () => {
                    OnTableFunctionChanged(PageChange, this.state);
                  })
              }} className={currentPage === i ? 'page-item active' : 'page-item'}><a id={`${i}`} className="page-link">{i}</a></li>)
          }
      }

      return component;
  }

  render () {
    const { OnTableFunctionChanged, page, count, perPage, fields, button, filter, data, jenis } = this.props;
    const maxPage = Math.floor((count / perPage)) + ((count % perPage) > 0 ? 1 : 0);
    
    return (
      <div>
        <Row className="pt-12 containersBody"> 
          <Col style={{ padding: '2px' }}>
            {
              filter !== undefined ?
              filter : null
            }
            <Row>
              {
                button !== undefined ?
                  <div>
                    { button }
                  </div>
                : null
              }
            </Row>          
          </Col>
        </Row>
        <Card>
          <Table jenis={jenis} data={data} fields={fields} onChangeTable={this.props.OnTableFunctionChanged} />
        </Card>
        <Col xs="12" style={{ padding: '0 0 0 0' }}>
          <div class="row">
              <Col>
                  <ul className="pagination justify-content-end">
                      <li className="page-item disabled"><a className="page-link">{`Page ${page} of ${maxPage} (Total ${count})`}</a></li>
                      <li onClick={() => { // Click to Previous Page
                          if (page === 1) {
                              return;
                          }
                          const tmpPage = page;
                          this.setState({
                              ...this.state,
                              page: tmpPage - 1
                          }, () => {
                            OnTableFunctionChanged(PageChange, this.state);
                          })
                      }} className={page === 1 ? 'page-item disabled' : 'page-item'}><a className="page-link"><i className="fa fa-angle-left" /></a></li>
                      {
                          this.renderingPaging(maxPage, page)   
                      }
                      
                      <li onClick={() => { // Click to Next Page
                          if (page >= maxPage) {
                              return;
                          }
                          const tmpPage = page;
                          this.setState({
                              ...this.state,
                              page: tmpPage + 1
                          }, () => {
                            OnTableFunctionChanged(PageChange, this.state);
                          })
                      }} className={page >= maxPage ? 'page-item disabled' : 'page-item'}><a className="page-link"><i className="fa fa-angle-right" /></a></li>
                  </ul>
              </Col>
          </div>
        </Col>
      </div>
    );
  }
}

export default Datatable;