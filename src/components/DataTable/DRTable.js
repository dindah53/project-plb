import React, { Component } from 'react';
import { DeleteData, EditData } from '../../constants/Constants';
import { Table as TB } from 'reactstrap';

class Table extends Component {
    constructor (prop) {
        super(prop);
        this.state = {
        }
    }

    _onClickDel = (e) => {
        const { onChangeTable } = this.props;
        onChangeTable(DeleteData, e)
    }

    _editData = (e) => {
        const { onChangeTable } = this.props;
        onChangeTable(EditData, e)
    }

    render () {
        const { fields, data, jenis, viewRow } = this.props;
        let listField = fields || [];

        return (
            <div style={{ position: 'relative' }}>
                <TB>
                    <thead>
                        <tr>
                            {
                                listField.map((value) => {
                                    return <th className="t-head__low"> {value} </th>
                                })
                            }
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data ?
                            jenis === 'depo' ?
                            data.map(value => {
                                return (
                                    <tr className="t-row__low">
                                        <td>{value.id}</td>
                                        <td>{value.nama}</td>
                                        <td value={value.id} onClick={() => this._editData(value.id)}> <img style={{ width: 15 }} src={`${window.document.referrer}assets/logo/pensil.png`} /></td>
                                        <td className="" value={value.id} onClick={() => this._onClickDel(value.id)}><i className='fa fa-trash fa-lg' style={{ marginLeft: 10 }} /></td>
                                    </tr>
                                )
                            })
                            : jenis === 'rak' || jenis === 'perusahaan' ?
                            data.map(value => {
                                return (
                                    <tr className="t-row__low">
                                        <td>{value.kode_rak || value.kode_Perusahaan}</td>
                                        <td>{value.nama || value.nama_Perusahaan}</td>
                                        <td>{value.keterangan || value.alamat}</td>
                                        <td value={value.id} onClick={() => this._editData(value.id)}> <img style={{ width: 15 }} src={`${window.document.referrer}assets/logo/pensil.png`} /></td>
                                        <td className="" value={value.id} onClick={() => this._onClickDel(value.id)}><i className='fa fa-trash fa-lg' style={{ marginLeft: 10 }} /></td>
                                    </tr>
                                )
                            })
                            : jenis === 'pelabuhan' || jenis === 'ship' ?
                                data.map(value => {
                                    return (
                                        <tr className="t-row__low">
                                            <td>{value.kode_Pelabuhan || value.shipping_Line}</td>
                                            <td>{value.nama || value.kapal}</td>
                                            <td>{value.jenis || value.email}</td>
                                            <td>{value.kode_Negara || value.ccEmail}</td>
                                            <td>{value.nama_Negara || value.keterangan}</td>
                                            <td value={value.id} onClick={() => this._editData(value.id)}> <img style={{ width: 15 }} src={`${window.document.referrer}assets/logo/pensil.png`} /></td>
                                            <td className="" value={value.id} onClick={() => this._onClickDel(value.id)}><i className='fa fa-trash fa-lg' style={{ marginLeft: 10 }} /></td>
                                        </tr>
                                    )
                                }) 
                            :
                            <tr className="t-row__low">
                                <td></td>
                                <td></td>
                                <td> <img style={{ width: 15 }} src={`${window.document.referrer}assets/logo/pensil.png`} /></td>
                                <td><i className='fa fa-trash fa-lg' style={{ marginLeft: 10 }} /></td>
                            </tr>
                            : null
                        }
                    </tbody>
                </TB>
            </div>
        );
    }
}

export default Table;