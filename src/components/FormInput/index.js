import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Input } from 'reactstrap';
import InputDetailIdentitas from '../../views/Pages/DokumenMasuk/Dokumen/InputDetailIdentitas';
import InputDoct from '../../views/Pages/DokumenMasuk/Dokumen/InputDoct';
import InputDoctAju from '../../views/Pages/DokumenMasuk/Dokumen/InputDoctAju';
import { StateNext, StateSubmit } from '../../constants/Constants';

class FormInput extends Component {
  constructor (props) {
    super(props);
    this.state = {
        data: '',
        step: 1,
        dataAju: '',
        dataDetail: ''
    }
  }

  _handleNextStep = async () => {
      this.setState({
          step: this.state.step + 1
      })
      await this._renderPage()
  }

  _handlePreviousStep = async () => {
      this.setState({
          step: this.state.step - 1
      })
      await this._renderPage()
  }

  _handleBatalInput = () => {
    this.props.history.push('/dokumenmasuk-dokumen'); 
  }

  _handleSubmit = (e) => {
    const { dataAju, dataDetail } = this.state;
    let data = {
      ...dataAju,
      ...dataDetail,
      ...e
    }
    console.log(data)
  }

  _renderPage = () => {
    const { step } = this.state;
    if (step === 1) {
      return (
        <InputDoctAju next={ () => this._handleNextStep()} batal={ () => this._handleBatalInput()} 
        onChange={ async (state, data) => {
          console.log(data)
          switch (state) {
            case StateNext:
              this.setState({
                dataAju: data
              })
              break;
            default:
          }
        }}
        />
      )
    } else if (step === 2) {
      return (
        <InputDetailIdentitas next={ () => this._handleNextStep()} previous={ () => this._handlePreviousStep()} 
        onChange={ async (state, data) => {
          console.log(data)
          switch (state) {
            case StateNext:
              this.setState({
                dataDetail: data
              })
              break;
            default:
          }
        }}
        />
      )
    } else if (step === 3) {
      return (
        <InputDoct previous={ () => this._handlePreviousStep()} submit={ () => this.props.history.push('/dokumenmasuk-dokumen') }
        onChange={ async (state, data) => {
          console.log(data)
          switch (state) {
            case StateSubmit:
              this.setState({
                dataDokumen: data
              })
              await this._handleSubmit(data)
              break;
            default:
          }
        }}
        />
      )
    }
  }

  componentWillMount = () => {
      const { location } = this.props;
      if (location.data) {
          this._renderPage()
      } else {
        this.props.history.push('/dokumenmasuk-dokumen');
      }
  }

  render () {

    return (
        <div>
          {
            this._renderPage()
          }
        </div>
    )
  }
}

export default FormInput;