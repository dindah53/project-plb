import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Input } from 'reactstrap';
import 'react-datepicker/dist/react-datepicker.css';

class Calender extends Component {
  constructor (props) {
    super(props);
    this.state = {
        tanggal: ''
    }
  }
  
  _try = () => {
    window.scroll({
      top: 280,
      left: 0,
      behavior: 'smooth'
    })
  }

  _calendarInput = (value) => {
    const { style, className } = this.props;
    return (
      <Input onClick={() => this._try()} style={style} className={className} >
        {value}
      </Input>

    )
  }

  render () {

    return (
            <DatePicker
            dateFormat='dd-MM-yyyy'
            showMonthDropdown
            showYearDropdown
            selected={this.state.tanggal}
            onChange={date => this.setState({ tanggal: date })}
            placeholderText='DD-MM-YYYY'
            customInput={this._calendarInput()}
            popperPlacement="bottom"
            fixedHeight= {true}
            />
    )
  }
}

export default Calender;