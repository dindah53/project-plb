import{
    AR_REQLASTUPDATE,
    AR_SUCCESSLASTUPDATE,
    AR_FAILEDLASTUPDATE,
    OT_REQLASTUPDATE,
    OT_SUCCESSLASTUPDATE,
    OT_FAILEDLASTUPDATE
}from '../constants/Constants';

const ARLastUpdate = (state = {}, action) => {
    
    switch(action.type){
        case AR_REQLASTUPDATE:
            return{
                ...state,
                statusRequestAt: 'PENDING'
            }
        case AR_SUCCESSLASTUPDATE:
            return{
                ...state,
                statusRequestAt: 'SUCCESS',
                arlastupdate: action.payload
            }
        case AR_FAILEDLASTUPDATE:
            return{
                ...state,
                statusRequestAt: `FAILED: ${action.message}`
            }
        case OT_REQLASTUPDATE:
            return{
                ...state,
                statusRequestAt: 'PENDING'
            }
        case OT_SUCCESSLASTUPDATE:
            return{
                ...state,
                statusRequestAt: 'SUCCESS',
                arlastupdate: action.payload
            }
        case OT_FAILEDLASTUPDATE:
            return{
                ...state,
                statusRequestAt: `FAILED: ${action.message}`
            }
        default:
            return state;
    }
};

export default ARLastUpdate;