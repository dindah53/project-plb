
import { 
    GET_DEPO,
    FAILED_GET_DEPO
 } from '../constants/Constants';

const INITIAL_STATE = {
    listDepo: []
}

const MasterReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_DEPO:
            return {
                ...state,
                listDepo: action.payload
            }
        case FAILED_GET_DEPO:
            return {
                ...state,
                message: action.data
            }
        default:
            return state;
    }
}

export default MasterReducer;
