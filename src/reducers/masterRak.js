
import { 
    GET_RAK
 } from '../constants/Constants';

const INITIAL_STATE = {
    listRak: []
}

const MasterRakReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_RAK:
            return {
                ...state,
                listRak: action.payload
            }
        default:
            return state;
    }
}

export default MasterRakReducer;
