import MasterReducer from './masterDepo';
import MasterRakReducer from './masterRak';
import MasterAllReducer from './master';

export {
    MasterReducer,
    MasterRakReducer,
    MasterAllReducer
}