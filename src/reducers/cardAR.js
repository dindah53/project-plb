import {
    AR_REQCARD,
    AR_SUCCESSCARD,
    AR_FAILEDCARD,
    UNAUTHORIZED
}from '../constants/Constants';

const ARcard = (state = {}, action) => {
    switch(action.type){
        case AR_REQCARD :
            return{
                ...state,
                isRequestArCard: true
            }
        case AR_SUCCESSCARD:
            return{
                ...state,
                arcardError: false,
                arcard: action.payload,
                isRequestArCard: false
            }
        case AR_FAILEDCARD:
            return{
                ...state,
                arcardError: true,
                isRequestArCard: false
            }
        case UNAUTHORIZED:
            return {
                ...state,
                needLogin: true
            }
        default:
            return state;
    }
};

export default ARcard;