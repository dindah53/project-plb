import { 
    USER_AUTH_REQUEST, 
    USER_AUTH_SUCCESS, 
    USER_AUTH_INVALIDATE, 
    USER_AUTH_FAILED
} from '../constants/Constants';

const INITIATE = {
    username: '',
    password: '',
    isLogin: false
}

const auth = (state = INITIATE, action) => {
    switch (action.type) {
        case USER_AUTH_REQUEST:
            return {
                isLogin: true
            };
        case USER_AUTH_SUCCESS:
            return {
                isLogin: false,
                resp: action.resp
            };
        case USER_AUTH_FAILED:
            return {
                isLogin: false,
                error: action.error,
                message: action.message
            };
        case USER_AUTH_INVALIDATE:
            return {
                isLogin: false,
                error: false,
                message: '',
                resp: null
            };
        default:
            return state;
    }
}

export default auth;