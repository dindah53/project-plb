    import { INVALIDATE_ORDERTRACKING_TDL, 
    INVALIDATE_USERINFO, 
    REQUEST_ORDERTRACKING_TDL, 
    REQUEST_USERINFO, 
    SUCCESS_ORDERTRACKING_TDL, 
    SUCCESS_USERINFO,
    FAILED_ORDERTRACKING_TDL, 
    FAILED_USERINFO, 
    UNAUTHORIZED,
    ListAttribute
} from '../constants/Constants';

const ordertrackingtodolist = (state = {}, action) => {
    switch (action.type) {
        case INVALIDATE_ORDERTRACKING_TDL:
            return {
                ...state,
                ordertrackingError: false,
                ordertracking: {
                    data: [],
                    pageIndex: 0,
                    pageSize: 0,
                    totalCount: 0,
                    totalPageCount: 0
                }
            }
        case INVALIDATE_USERINFO:
            return {
                ...state,
                userinfoError: false,
                userinfo: {}
            }
        case REQUEST_ORDERTRACKING_TDL:
            return {
                ...state,
                isRequestOrderTrackingTodolist: true
            }
        case REQUEST_USERINFO:
            return {
                ...state,
                isRequestUserinfo: true
            }
        case SUCCESS_ORDERTRACKING_TDL:
            return {
                ...state,
                ordertrackingError: false,
                isRequestOrderTrackingTodolist: false,
                ordertracking: action.data
            }
        case ListAttribute:
            return {
                ...state,
                ordertrackingError: false,
                isRequestOrderTrackingTodolist: false,
                listAttribute: action.data
            }
        case SUCCESS_USERINFO:
            return {
                ...state,
                userinfoError: false,
                isRequestUserinfo: false,
                userinfo: action.data
            }
        case FAILED_ORDERTRACKING_TDL:
            return {
                ...state,
                ordertrackingError: true,
                isRequestOrderTrackingTodolist: false,
                message: action.data
            }
        case FAILED_USERINFO:
            return {
                ...state,
                userinfoError: true,
                isRequestUserinfo: false,
                message: action.data
            }
        case UNAUTHORIZED:
            return {
                ...state,
                needLogin: true
            }
        default:
            return state;
    }
}

export default ordertrackingtodolist;