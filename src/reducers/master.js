
import { 
    GET_PELABUHAN,
    GET_LIST_NEGARA,
    GET_LIST_PERUSAHAAN,
    GET_SHIPPINGLINE
 } from '../constants/Constants';

const INITIAL_STATE = {
    listPelabuhan: [],
    listNegara: [],
    listPerusahaan: [],
    listShippingline: []
}

const MasterAllReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PELABUHAN:
            return {
                ...state,
                listPelabuhan: action.payload
            }
        case GET_LIST_NEGARA:
            return {
                ...state,
                listNegara: action.payload
            }
        case GET_LIST_PERUSAHAAN:
            return {
                ...state,
                listPerusahaan: action.payload
            }
        case GET_SHIPPINGLINE:
            return {
                ...state,
                listShippingline: action.payload
            }
        default:
            return state;
    }
}

export default MasterAllReducer;
