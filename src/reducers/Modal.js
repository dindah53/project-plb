const INITIAL_STATE = {
    title: '', 
    body: '', 
    isOpen: false,
    onClose: () => {},
    toggleModal: () => {},
    onAction: {
        confirm: {},
        cancel: {}
    }
}

const Modal = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case 'OPEN_MODAL':
            return {
                ...state,
                isOpen: true,
                isRequest: true,
                body: action.body,
                title: action.title,
                onClose: action.onClose,
                onAction: {
                    ...state.onAction,
                    confirm: action.onAction.confirm,
                    cancel:  action.onAction.cancel
                }
            }
        case 'CLOSE_MODAL':
            return {
                ...state,
                isOpen: false,
                isRequest: false
            }
        default:
            return state;
    }
}

export default Modal;