import {
    AR_REQ, 
    AR_SUCCESS, 
    AR_FAILED,
    AR_REQCARD,
    AR_SUCCESSCARD,
    AR_FAILEDCARD,
    UNAUTHORIZED
}from '../constants/Constants';

const ARlist = (state = {}, action) => {
    switch(action.type){
        case AR_REQ :
            return{
                ...state,
                isRequestArList: true
            }
        case AR_SUCCESS:
            return{
                ...state,
                arListError: false,
                arlist: action.payload,
                isRequestArList: false
            }
        case AR_FAILED:
            return{
                ...state,
                arListError: true,
                isRequestArList: false
            }
        case UNAUTHORIZED:
            return {
                ...state,
                needLogin: true
            }
        default:
            return state;
    }
};

export default ARlist;
