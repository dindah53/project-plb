const navigation = {
  items: [
    {
      name: 'Home',
      url: '/home',
      icon: 'icon-home'
    },
    {
      name: 'Master Data',
    //  url: '',
      icon: 'fa fa-database',
      children: [
        {
          name: 'Daftar Depo',
          url: '/master-depo'
        },
        {
          name: 'Daftar Rak',
          url: '/master-rak'
        },
        {
          name: 'Daftar Pelabuhan',
          url: '/master-pelabuhan'
        },
        {
          name: 'Daftar Perusahaan',
          url: '/master-perusahaan'
        },
        {
          name: 'Daftar Shippingline',
          url: '/master-shippingline'
        }
      ]
    },
    {
       name: 'Dokumen Masuk',
       // url: '',
       icon: 'fa fa-file-o',
        children: [
          {
            name: 'Dokumen',
            url: '/dokumenmasuk-dokumen'
          }
          // {
          //   name: 'Realisasi Masuk',
          //   url: ''
          // },
          // {
          //   name: 'Buang Empty',
          //   url: ''
          // }
        ]
    }
    // ,
    // {
    //   name: 'Dokumen Keluar',
    //   url: '',
    //   icon: 'fa fa-print',
    //   children: [
    //     {
    //       name: 'Dokumen',
    //       url: ''
    //     },
    //     {
    //       name: 'Realisasi Keluar',
    //       url: ''
    //     }
    //   ]
    // },
    // {
    //   name: 'Dokumen Batal',
    //   url: '',
    //   icon: 'fa fa-trash'
    // },
    // {
    //   name: 'Inventory',
    //   url: '',
    //   icon: 'fa fa-refresh',
    //   children: [
    //     {
    //       name: 'Data Stok',
    //       url: ''
    //     },
    //     {
    //       name: 'Stok Opname',
    //       url: ''
    //     }
    //   ]
    // },
    // {
    //   name: 'Keuangan',
    //   url: '',
    //   icon: 'fa fa-money',
    //   children: [
    //     {
    //       name: 'Data Tarif',
    //       url: ''
    //     },
    //     {
    //       name: 'Data Invoice',
    //       url: ''
    //     },
    //     {
    //       name: 'Data Reimburse',
    //       url: ''
    //     }
    //   ]
    // },
    // {
    //   name: 'Laporan',
    //   url: '',
    //   icon: 'fa fa-book',
    //   children: [
    //     {
    //       name: 'Koneksi Dokumen',
    //       url: ''
    //     },
    //     {
    //       name: 'Dokumen BC 1.8',
    //       url: ''
    //     },
    //     {
    //       name: 'Dokumen BC 2.6',
    //       url: ''
    //     },
    //     {
    //       name: 'Stok Opname',
    //       url: ''
    //     },
    //     {
    //       name: 'Dokumen CBM',
    //       url: ''
    //     }
    //   ]
    // },
    // {
    //   name: 'Administrasi User',
    //   url: '',
    //   icon: 'fa fa-address-book-o'
    // }
  ]
};

export default navigation;
