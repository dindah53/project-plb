import React, { Component } from 'react';
import { Route, Switch, Router, Redirect, BrowserRouter, HashRouter } from 'react-router-dom';
import { history } from './helpers';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { Provider } from 'react-redux';
import './App.scss';
import './App.css';
import Login from './views/Pages/Login';
import DefaultLayout from './containers/DefaultLayout';
import { MasterReducer, MasterRakReducer, MasterAllReducer } from './reducers/index';

const reducer = combineReducers({
    MasterReducer, 
    MasterRakReducer, 
    MasterAllReducer
});

const store = createStore(
    reducer,
    applyMiddleware(thunkMiddleware)
);

class App extends Component {
    render () {
      return (
        <Provider store={store}>
            <div>
                <HashRouter history={history}>
                    <div>
                        <Switch>
                            <Route path="/login" component={Login} />
                            <Route path="/" render={(props) => (
                                localStorage.getItem('user') ? 
                                (<DefaultLayout {...props} />) : ((<Redirect to='/login' />))
                            )} />
                        </Switch>
                    </div>
                </HashRouter>
            </div>
        </Provider>
      );
    }
  }
  export default App;