import Home from '../src/views/Pages/home/home';
import MasterDaftarDepo from './views/Pages/DataMaster/DaftarDepo/index'
import MasterDaftarRak from './views/Pages/DataMaster/DaftarRak/index';
import MasterDaftarPelabuhan from './views/Pages/DataMaster/DaftarPelabuhan/index';
import MasterDaftarPerusahaan from './views/Pages/DataMaster/DaftarPerusahaan/index';
import MasterDaftarShippingline from './views/Pages/DataMaster/DaftarShippingline/index';
import Dokumen from './views/Pages/DokumenMasuk/Dokumen/index';
import FormInput from './components/FormInput/index';

const routes = [
    { path: '/', exact: true, name: 'Home', component: Home },
    { path: '/home', exact: true, name: 'Home', component: Home },
    { path: '/master-depo', exact: true, name: 'Daftar Depo', component: MasterDaftarDepo },
    { path: '/master-rak', exact: true, name: 'Daftar Rak', component: MasterDaftarRak },
    { path: '/master-pelabuhan', exact: true, name: 'Daftar Pelabuhan', component: MasterDaftarPelabuhan },
    { path: '/master-perusahaan', exact: true, name: 'Daftar Perusahaan', component: MasterDaftarPerusahaan },
    { path: '/master-Shippingline', exact: true, name: 'Daftar Shippingline', component: MasterDaftarShippingline },
    { path: '/dokumenmasuk-dokumen', exact: true, name: 'Daftar Dokumen', component: Dokumen },
    { path: '/dokumenmasuk-dokumen/input', exact: true, name: 'Input Dokumen', component: FormInput }
]

export default routes;