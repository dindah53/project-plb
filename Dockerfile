# build environment
FROM node:12.2.0-alpine as build
WORKDIR /app

ARG REACT_APP_BASE_LOGIN
ARG REACT_APP_BASE_INTERNAL
ARG REACT_APP_BASE_AR
ARG REACT_APP_XIBM_LOGIN
ARG REACT_APP_XIBM_ATTRIBUTE

ENV REACT_APP_BASE_LOGIN=$REACT_APP_BASE_LOGIN
ENV REACT_APP_BASE_INTERNAL=$REACT_APP_BASE_INTERNAL
ENV REACT_APP_BASE_AR=$REACT_APP_BASE_AR
ENV REACT_APP_XIBM_LOGIN=$REACT_APP_XIBM_LOGIN
ENV REACT_APP_XIBM_ATTRIBUTE=$REACT_APP_XIBM_ATTRIBUTE

ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent
COPY . /app
RUN npm run build

# production environment
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]